package com.bank.bankApplication.enity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class UserTransaction implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String transactionNo;

	private Date transactionDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "from_user_id", nullable = false)
	private UserAccount fromAccount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "to_user_id", nullable = false)
	private UserAccount toAccount;

	private Double transferedAmount;

	private Double fromAccountBalance;

	private Double toAccountBalance;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public UserAccount getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(UserAccount fromAccount) {
		this.fromAccount = fromAccount;
	}

	public UserAccount getToAccount() {
		return toAccount;
	}

	public void setToAccount(UserAccount toAccount) {
		this.toAccount = toAccount;
	}

	public Double getTransferedAmount() {
		return transferedAmount;
	}

	public void setTransferedAmount(Double transferedAmount) {
		this.transferedAmount = transferedAmount;
	}

	public Double getFromAccountBalance() {
		return fromAccountBalance;
	}

	public void setFromAccountBalance(Double fromAccountBalance) {
		this.fromAccountBalance = fromAccountBalance;
	}

	public Double getToAccountBalance() {
		return toAccountBalance;
	}

	public void setToAccountBalance(Double toAccountBalance) {
		this.toAccountBalance = toAccountBalance;
	}

}