package com.bank.bankApplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.bankApplication.enity.UserTransaction;

@Repository
public interface UserTransactionRepository extends JpaRepository<UserTransaction, Long> {

	List<UserTransaction> findByFromAccountAccountNumberOrToAccountAccountNumber(String fromAccount, String toAccount);

}
