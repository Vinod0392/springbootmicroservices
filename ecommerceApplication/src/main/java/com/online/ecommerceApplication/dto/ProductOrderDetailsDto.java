package com.online.ecommerceApplication.dto;

import java.io.Serializable;

public class ProductOrderDetailsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String productName;

	private Double productPrice;

	private Integer productQuantity;

	public ProductOrderDetailsDto(String productName, Double productPrice, Integer productQuntity) {
		super();
		this.productName = productName;
		this.productPrice = productPrice;
		this.productQuantity = productQuantity;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}

	public Integer getProductQuntity() {
		return productQuantity;
	}

	public void setProductQuntity(Integer productQuntity) {
		this.productQuantity = productQuntity;
	}

}
