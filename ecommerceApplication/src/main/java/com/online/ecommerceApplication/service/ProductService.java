package com.online.ecommerceApplication.service;

import org.springframework.http.ResponseEntity;

public interface ProductService {

	ResponseEntity<?> getProductByProductnameOrCategoryName(String productName, String categoryName);

}
