package com.bank.bankApplication.service;

import org.springframework.http.ResponseEntity;

import com.bank.bankApplication.enity.User;

public interface UserRegistrationService {

	ResponseEntity<?> userRegistrationForm(final User user);

}
