package com.online.ecommerceApplication.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "http://BANKAPPLICATION/bank")
public interface BankApplicationClient {

	@PostMapping("/fundTransfer")
	public ResponseEntity<?> bankTransfer(@RequestBody PaymentRequest payment);

}
