package com.online.ecommerceApplication.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.online.ecommerceApplication.entity.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
