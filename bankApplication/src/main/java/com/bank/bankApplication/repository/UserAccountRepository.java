package com.bank.bankApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.bankApplication.enity.UserAccount;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, String> {

	UserAccount findByAccountNumber(String accountNumber);

}
