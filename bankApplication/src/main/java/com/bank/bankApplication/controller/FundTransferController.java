package com.bank.bankApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.bankApplication.dto.PaymentRequestDTO;
import com.bank.bankApplication.service.UserTranasctionService;

@RestController
@RequestMapping("/bank")
public class FundTransferController {

	@Autowired
	UserTranasctionService tranasctionService;

	@PostMapping(path = "/fundTransfer")
	public ResponseEntity<?> fundTransfer(@RequestBody PaymentRequestDTO payment) {
		ResponseEntity<?> responseEntity = tranasctionService.fundTransfer(payment);
		return responseEntity;
	}

}
