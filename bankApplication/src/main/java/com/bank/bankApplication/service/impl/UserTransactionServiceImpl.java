package com.bank.bankApplication.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bank.bankApplication.dto.PaymentRequestDTO;
import com.bank.bankApplication.dto.PaymentResponseDTO;
import com.bank.bankApplication.dto.StatmentReportDTO;
import com.bank.bankApplication.dto.UserStatmentDTO;
import com.bank.bankApplication.enity.UserAccount;
import com.bank.bankApplication.enity.UserTransaction;
import com.bank.bankApplication.repository.UserAccountRepository;
import com.bank.bankApplication.repository.UserRepository;
import com.bank.bankApplication.repository.UserTransactionRepository;
import com.bank.bankApplication.service.UserTranasctionService;

@Service
public class UserTransactionServiceImpl implements UserTranasctionService {

	@Autowired
	UserTransactionRepository transactionRepository;

	@Autowired
	UserRepository appUserRepository;

	@Autowired
	UserAccountRepository accountRepository;

	@Override
	public ResponseEntity<?> statementReport(final Integer month, final Integer year, final String accountNumber) {
		UserAccount account = accountRepository.findByAccountNumber(accountNumber);
		if (accountNumber == null) {
			return new ResponseEntity<>("Account Not Found", HttpStatus.BAD_REQUEST);
		}
		if (month == null || month > 11 || month < 0) {
			return new ResponseEntity<>("Invalid Month...", HttpStatus.BAD_REQUEST);
		}
		List<UserTransaction> transactions = transactionRepository
				.findByFromAccountAccountNumberOrToAccountAccountNumber(accountNumber, accountNumber);
		StatmentReportDTO statementReport = generateStatementSummaryReport(month, year, accountNumber, account,
				transactions);
		return new ResponseEntity<>(statementReport, HttpStatus.OK);
	}

	@Override
	@Transactional
	public ResponseEntity<?> fundTransfer(final PaymentRequestDTO payment) {
		if (payment.getPayAmount() > 0.0) {
			UserTransaction tranasction = new UserTransaction();
			tranasction.setTransactionDate(new Date());
			UserAccount fromAccount = accountRepository.findByAccountNumber(payment.getFromAccount());
			UserAccount toAccount = accountRepository.findByAccountNumber(payment.getToAccount());
			if (fromAccount == null || toAccount == null) {
				return new ResponseEntity<>("Payment failed !! Account Not Found", HttpStatus.BAD_REQUEST);
			}
			if (fromAccount.getBalance() < payment.getPayAmount()) {
				return new ResponseEntity<>("Payment failed !! Insufficient balance ", HttpStatus.BAD_REQUEST);
			}
			tranasction.setFromAccount(fromAccount);
			tranasction.setToAccount(toAccount);
			tranasction.setTransferedAmount(payment.getPayAmount());
			String transactionNo = RandomStringUtils.randomNumeric(10).toString();
			tranasction.setTransactionNo(transactionNo);
			double fromAccountBalance = fromAccount.getBalance() - payment.getPayAmount();
			double toAccountBalance = toAccount.getBalance() + payment.getPayAmount();
			tranasction.setFromAccountBalance(fromAccountBalance);
			tranasction.setToAccountBalance(toAccountBalance);
			transactionRepository.save(tranasction);
			fromAccount.setBalance(fromAccountBalance);
			toAccount.setBalance(toAccountBalance);
			accountRepository.save(fromAccount);
			accountRepository.save(toAccount);
			
			PaymentResponseDTO paymentResponse = new PaymentResponseDTO(transactionNo, payment.getPayAmount());
			return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(" Payment failed !!", HttpStatus.BAD_REQUEST);
		}
	}

	private StatmentReportDTO generateStatementSummaryReport(final Integer month, final Integer year,
			final String accountNumber, UserAccount account, List<UserTransaction> transactions) {
		StatmentReportDTO statementReport = new StatmentReportDTO();
		statementReport.setBalance(account.getBalance());
		statementReport.setAccountNumber(accountNumber);
		statementReport.setAccountCreatedDate(account.getCreatedDate());
		List<UserStatmentDTO> transactionStatment = new ArrayList<>();
		for (UserTransaction transaction : transactions) {
			Date txnDate = transaction.getTransactionDate();
			Calendar requestCal = Calendar.getInstance();
			requestCal.setTime(txnDate);
			if (requestCal.get(Calendar.YEAR) == year && requestCal.get(Calendar.MONTH) == month) {
				UserStatmentDTO statementInfo = new UserStatmentDTO();
				if (transaction.getToAccount().getAccountNumber().equals(accountNumber)) {
					statementInfo.setCreditedAmount(transaction.getTransferedAmount());
					statementInfo.setBalance(transaction.getToAccountBalance());
				} else if (transaction.getFromAccount().getAccountNumber().equals(accountNumber)) {
					statementInfo.setDebitedAmount(transaction.getTransferedAmount());
					statementInfo.setBalance(transaction.getFromAccountBalance());
				}
				statementInfo.setTransactionNo(transaction.getTransactionNo());
				statementInfo.setTransactionDate(transaction.getTransactionDate().toString());
				transactionStatment.add(statementInfo);
			}
		}
		statementReport.setTransactionHistory(transactionStatment);
		return statementReport;
	}

}
