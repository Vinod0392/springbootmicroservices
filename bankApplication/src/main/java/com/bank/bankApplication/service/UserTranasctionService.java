package com.bank.bankApplication.service;

import org.springframework.http.ResponseEntity;

import com.bank.bankApplication.dto.PaymentRequestDTO;

public interface UserTranasctionService {

	ResponseEntity<?> statementReport(final Integer month, final Integer year, final String accountNumber);

	ResponseEntity<?> fundTransfer(final PaymentRequestDTO payment);

}
