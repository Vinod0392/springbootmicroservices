package com.bank.bankApplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.bankApplication.enity.User;
import com.bank.bankApplication.service.UserRegistrationService;

@RestController
@RequestMapping("/bank")
public class UserRegistrationController {

	@Autowired
	UserRegistrationService userRegistrationService;

	@PostMapping("/userRegistration")
	public ResponseEntity<?> userRegistration(@Valid @RequestBody User appUser) {
		return userRegistrationService.userRegistrationForm(appUser);
	}

}
