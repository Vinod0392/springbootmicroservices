package com.online.ecommerceApplication.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.online.ecommerceApplication.entity.OrderDetails;

@Repository
public interface OrderDetailsRepository extends PagingAndSortingRepository<OrderDetails, Long> {

	List<OrderDetails> findByOrderCustomerCustomerId(Long customerId);
}
