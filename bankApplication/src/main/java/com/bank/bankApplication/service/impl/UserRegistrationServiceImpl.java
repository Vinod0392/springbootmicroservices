package com.bank.bankApplication.service.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bank.bankApplication.enity.User;
import com.bank.bankApplication.enity.UserAccount;
import com.bank.bankApplication.repository.UserRepository;
import com.bank.bankApplication.service.UserRegistrationService;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional
	public ResponseEntity<?> userRegistrationForm(final User user) {
		System.out.println("testing the code........");
		if (user == null) {
			return new ResponseEntity<>(" Invalid Details !!", HttpStatus.BAD_REQUEST);
		}
		UserAccount account = user.getAccount();
		if (account == null) {
			return new ResponseEntity<>("Invalid Account details... ", HttpStatus.BAD_REQUEST);
		}
		if (account.getBalance() == null || account.getBalance() < 0.0) {
			return new ResponseEntity<>(" Invalid Balance... ", HttpStatus.BAD_REQUEST);
		}
		List<User> users = userRepository.findByEmailAddress(user.getEmailAddress());
		if (!users.isEmpty()) {
			return new ResponseEntity<>(" User Found with Same email address... ", HttpStatus.BAD_REQUEST);
		}
		users = userRepository.findByAadharCard(user.getAadharCard());
		if (!users.isEmpty()) {
			return new ResponseEntity<>("User Found with Same Aadhar card", HttpStatus.BAD_REQUEST);
		}

		account.setCreatedDate(new Date());
		if (account.getBalance() == null || account.getBalance() == 0.0) {
			account.setBalance(5000.0);
		}
		account.setAccountNumber(RandomStringUtils.randomNumeric(10).toString());
		account.setUser(user);
		userRepository.save(user);
		return new ResponseEntity<>("Customer account created successfully ", HttpStatus.OK);
	}

}
