package com.online.ecommerceApplication.service;

import org.springframework.http.ResponseEntity;

import com.online.ecommerceApplication.dto.PurchaseOrderRequestDto;

public interface OrderService {

	ResponseEntity<?> placeOrder(PurchaseOrderRequestDto orderRequest);

	ResponseEntity<?> orderHistory(Long customerId);
}
