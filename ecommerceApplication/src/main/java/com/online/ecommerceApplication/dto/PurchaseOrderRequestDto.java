package com.online.ecommerceApplication.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PurchaseOrderRequestDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Please enter the valid Customer Id")
	private Long customerId;

	@NotNull(message = "Please enter the valid Account number")
	@NotEmpty(message = "Please enter the valid Account number")
	private String accountNumber;

	@NotEmpty(message = "It is a invalid ProductItems")
	@NotNull(message = "It is a invalid ProductItems")
	private List<ProductItemsDto> productItems = new ArrayList<>();

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public List<ProductItemsDto> getProductItems() {
		return productItems;
	}

	public void setProductItems(List<ProductItemsDto> productItems) {
		this.productItems = productItems;
	}

}
