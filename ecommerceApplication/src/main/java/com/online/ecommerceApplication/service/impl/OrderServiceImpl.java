package com.online.ecommerceApplication.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.online.ecommerceApplication.dto.OrderDto;
import com.online.ecommerceApplication.dto.OrderHistoryDto;
import com.online.ecommerceApplication.dto.ProductItemsDto;
import com.online.ecommerceApplication.dto.ProductOrderDetailsDto;
import com.online.ecommerceApplication.dto.PurchaseOrderRequestDto;
import com.online.ecommerceApplication.entity.Customer;
import com.online.ecommerceApplication.entity.Order;
import com.online.ecommerceApplication.entity.OrderDetails;
import com.online.ecommerceApplication.entity.Product;
import com.online.ecommerceApplication.feignclient.BankApplicationClient;
import com.online.ecommerceApplication.feignclient.PaymentRequest;
import com.online.ecommerceApplication.repository.CustomerRepository;
import com.online.ecommerceApplication.repository.OrderDetailsRepository;
import com.online.ecommerceApplication.repository.OrderRepository;
import com.online.ecommerceApplication.repository.ProductRepository;
import com.online.ecommerceApplication.service.OrderService;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	OrderDetailsRepository orderDetailsRepository;

	@Autowired
	BankApplicationClient bankAppClient;

	private String systemAccountNumber = "13541002098657";

	@Override
	public ResponseEntity<?> placeOrder(final PurchaseOrderRequestDto orderRequest) {
		Set<Long> productIds = orderRequest.getProductItems().stream().map(ProductItemsDto::getProductId)
				.collect(Collectors.toSet());
		List<Product> selectedProducts = (List<Product>) productRepository.findAllById(productIds);
		for (ProductItemsDto item : orderRequest.getProductItems()) {
			if (item.getQuantity() == null) {
				return new ResponseEntity<>("Please enter the valid Quatity", HttpStatus.BAD_REQUEST);
			}
			if (item.getProductId() == null) {
				return new ResponseEntity<>("Please enter the valid Product Id", HttpStatus.BAD_REQUEST);
			}
		}
		ResponseEntity<?> orderValidationResponse = validatePurchaseOrder(orderRequest, productIds, selectedProducts);
		if (orderValidationResponse != null) {
			return orderValidationResponse;
		}
		Double orderAmount = selectedProducts.stream().mapToDouble(p -> p.getProductPrice()).sum();
		Order customerOrder = new Order();
		Optional<Customer> customer = customerRepository.findById(orderRequest.getCustomerId());
		customerOrder.setCustomer(customer.get());
		customerOrder.setOrderDate(new Date());
		customerOrder.setOrderNumber( RandomStringUtils.randomNumeric(20).toString());
		customerOrder.setOrderStatus(2);
		customerOrder.setOrderAmount(orderAmount);
		orderRepository.save(customerOrder);
		List<OrderDetails> orderDetailsList = new ArrayList<>();
		for (ProductItemsDto productItem : orderRequest.getProductItems()) {
			OrderDetails orderDetails = new OrderDetails();
			orderDetails.setOrder(customerOrder);
			Product productEntity = productRepository.findById(productItem.getProductId()).get();
			orderDetails.setProduct(productEntity);
			orderDetails.setProductQuantity(productItem.getQuantity());
			orderDetails.setProductPrice(productEntity.getProductPrice());
			orderDetailsList.add(orderDetails);
		}
		orderDetailsRepository.saveAll(orderDetailsList);
		PaymentRequest paymentRequest = new PaymentRequest(orderAmount, orderRequest.getAccountNumber(),
				systemAccountNumber);
		ResponseEntity<?> bankTransferResponse = bankAppClient.bankTransfer(paymentRequest);

		if (bankTransferResponse.getStatusCodeValue() <= 201) {
			customerOrder.setOrderStatus(1);
			orderRepository.save(customerOrder);
		}
		return new ResponseEntity<>(
				"Order is successfully Created  : order referance number " + customerOrder.getOrderNumber(), HttpStatus.OK);
	}

	private ResponseEntity<?> validatePurchaseOrder(PurchaseOrderRequestDto orderRequest, Set<Long> productIds,
			List<Product> selectedProducts) {
		Optional<Customer> customer = customerRepository.findById(orderRequest.getCustomerId());
		if (!customer.isPresent()) {
			return new ResponseEntity<>("Please enter the valid Customer Id", HttpStatus.BAD_REQUEST);
		}
		if (selectedProducts.isEmpty()) {
			return new ResponseEntity<>("Please enter the valid Products", HttpStatus.BAD_REQUEST);
		} else {
			for (Product product : selectedProducts) {
				Integer totalItemQty = orderRequest.getProductItems().stream()
						.filter(item -> item.getProductId().equals(product.getProductId()))
						.mapToInt(p -> p.getQuantity()).sum();
				if (totalItemQty > product.getAvailableQuantity()) {
					return new ResponseEntity<>("Please enter the valid Product Quantity", HttpStatus.BAD_REQUEST);
				}
			}
		}
		return null;
	}

	@Override
	public ResponseEntity<?> orderHistory(final Long customerId) {
		Optional<Customer> customer = customerRepository.findById(customerId);
		if (!customer.isPresent()) {
			return new ResponseEntity<>("Please enter the valid Customer Id", HttpStatus.BAD_REQUEST);
		} else {
			OrderHistoryDto orderHistoryResponse = new OrderHistoryDto();
			orderHistoryResponse.setCustomerId(customerId);
			orderHistoryResponse.setCustomerName(customer.get().getCustomerName());
			Set<Order> customerOrders = customer.get().getCustomerOrders().stream()
					.sorted(Comparator.comparing(Order::getOrderDate)).collect(Collectors.toSet());
			List<OrderDto> orders = new ArrayList<>();
			for (Order order : customerOrders) {
				List<ProductOrderDetailsDto> orderProductDetails = new ArrayList<>();
				for (OrderDetails orderDetail : order.getOrderDetails()) {
					Product product = orderDetail.getProduct();
					orderProductDetails.add(new ProductOrderDetailsDto(product.getProductName(),
							orderDetail.getProductPrice(), orderDetail.getProductQuantity()));
				}
				orders.add(new OrderDto(order.getOrderId(), order.getOrderNumber(), order.getOrderAmount(),
						order.getOrderDate(), orderProductDetails));
			}
			orderHistoryResponse.setOrders(orders);
			return new ResponseEntity<>(orderHistoryResponse, HttpStatus.OK);
		}
	}

}
