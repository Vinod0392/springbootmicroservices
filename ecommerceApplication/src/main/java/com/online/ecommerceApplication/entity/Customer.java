package com.online.ecommerceApplication.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long customerId;

	@NotNull(message ="Enter the Customer Name")
	@Size(min = 5, message = "The customer name is must be at least 5 characters")
	@Size(max = 15, message = "The customer name is must be less than 15 characters")
	private String customerName;

	@NotNull(message ="Please enter the valid mail address")
	@NotEmpty(message ="Please enter the valid mail address")
	private String emailAddress;
  
	@NotNull(message ="Please enter the valid mobile number")
	@NotEmpty(message ="Please enter the valid mobile number")
	private String mobileNumber;

	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnore
	private Set<Order> customerOrders = new HashSet<>();

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Set<Order> getCustomerOrders() {
		return customerOrders;
	}

	public void setCustomerOrders(Set<Order> customerOrders) {
		this.customerOrders = customerOrders;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
