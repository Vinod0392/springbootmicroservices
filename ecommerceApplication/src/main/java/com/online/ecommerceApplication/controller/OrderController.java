package com.online.ecommerceApplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.ecommerceApplication.dto.PurchaseOrderRequestDto;
import com.online.ecommerceApplication.service.OrderService;

@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@PostMapping("/placeOrder")
	public ResponseEntity<?> placeOrder(@Valid @RequestBody PurchaseOrderRequestDto orderRequest) {
		return orderService.placeOrder(orderRequest);
	}

	@GetMapping("/history/{customerId}")
	public ResponseEntity<?> orderHistory(@Valid @PathVariable(required = true) Long customerId) {
		return orderService.orderHistory(customerId);
	}

}
