package com.online.ecommerceApplication.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.online.ecommerceApplication.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

	List<Order> findByCustomerCustomerId(Long customerId);
}
