package com.bank.bankApplication.dto;

import java.io.Serializable;

public class PaymentResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String transactionNo;

	private Double paidAmount;

	public PaymentResponseDTO(String transactionNo, Double paidAmount) {
		super();
		this.transactionNo = transactionNo;
		this.paidAmount = paidAmount;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

}
