package com.online.ecommerceApplication.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.online.ecommerceApplication.entity.ProductCategory;

@Repository
public interface ProductCategoryRepository extends CrudRepository<ProductCategory, Long> {

	List<ProductCategory> findAllByCategoryName(String categoryName);

}
